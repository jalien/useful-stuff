import random
from threading import Thread
from queue import Queue
import download
from time import time
import sys


class DownloadWorker(Thread):

    def __init__(self, queue, outqueue_size, outqueue_bandwidth, download_dir, threadn, env_var):
        Thread.__init__(self)
        self.queue = queue
        self.outqueue_size = outqueue_size
        self.outqueue_bandwidth = outqueue_bandwidth
        self.download_dir = download_dir
        self.threadn = threadn
        self.env_var = env_var

    def run(self):
        while True:
            # Get the work from the queue and expand the tuple
            rand_pfn = self.queue.get()
            try:
                (size, bandwidth) = download.download_link(rand_pfn["pfn"], rand_pfn["envelope"],
                        self.download_dir, self.threadn, self.env_var)
                self.outqueue_size.put(size)
                self.outqueue_bandwidth.put(bandwidth)
            finally:
                self.queue.task_done()


def main():
    ts_setup = time()
    download_dir = download.setup_download_dir()
    if len(sys.argv) > 3:
        path = str(sys.argv[3])
    else:
        path = '/global/u2/a/alicepro/pfn_decoded.txt'

    pfns = download.get_dataset(path)
    # Creating a queue to communicate with the worker threads
    queue = Queue()
    outqueue_size = Queue()
    outqueue_bandwidth = Queue()
    # Creating worker threads
    (out, err) = download.run_cmd("TMPDIR=/tmp; /cvmfs/alice.cern.ch/bin/alienv printenv xjalienfs")
    for x in range(int(sys.argv[1])):
        worker = DownloadWorker(queue, outqueue_size, outqueue_bandwidth, download_dir, x, out)
        worker.daemon = True
        worker.start()
    random.shuffle(pfns)
    pfns = pfns[0:int(sys.argv[2])]
    ts = time()
    print("Total number of PFNS: " + str(len(pfns)))
    for rand_pfn in pfns:
        if bool(rand_pfn):
            print('Queueing {}'.format(rand_pfn["pfn"]))
            queue.put(rand_pfn)
    # Main thread waiting for the queue to finish processing all the tasks
    queue.join()
    totalsize = 0
    totalbandwidth = 0
    print("Starting to sum")

    for partialsize in outqueue_size.queue:
        totalsize = totalsize + partialsize

    for partialbandwidth in outqueue_bandwidth.queue:
        totalbandwidth = totalbandwidth + partialbandwidth

    bandwidth_per_thread_1 = round((totalbandwidth / outqueue_bandwidth.qsize()),2)
    bandwidth_total_1 = bandwidth_per_thread_1 * int(sys.argv[1])

    bandwidth_total_2 = round((totalsize / (time() - ts)),2)
    bandwidth_per_thread_2 = bandwidth_total_2 / int(sys.argv[1])

    print('Total size = ' + str(totalsize) + ' Bytes')
    print('Took ' + str(round((time() - ts),2)) + ' seconds')
    #print('Took setup ' + str(round((time() - ts_setup),2)) + ' seconds')
    print('Average per download bandwidth = ' + str(bandwidth_per_thread_1) + "B/s (" + download.humansize(bandwidth_per_thread_1) + "/s)")
    print('Whole node\'s bandwidth = ' + str(bandwidth_total_2) + "B/s (" + download.humansize(bandwidth_total_2) + "/s)")
    print('Per thread\'s bandwidth = ' + str(bandwidth_per_thread_2) + "B/s (" + download.humansize(bandwidth_per_thread_2) + "/s)")


if __name__ == '__main__':
    main()
