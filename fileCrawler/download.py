import json
import os
from pathlib import Path
import subprocess
import re
from time import time
import errno

def get_dataset(path):
    with open(path) as json_file:
        pfns = json.load(json_file)
        dataset_len = len(pfns)
        print(dataset_len)
        return pfns


def download_link(pfn, envelope, download_dir, threadn, env_var):
    #print (env_var)
    ts = time()
    cmd = "export " + str(env_var) + " xrdcp '" + pfn + "?authz=" + envelope + "' " + str(download_dir) + "/" + pfn.split('/')[-1] + " | tee -a output-thread-" + str(threadn)
    #print ("cmd is" + str(cmd))
    (out, err) = run_cmd(str(cmd))
    output =  err
    result = re.split(r"[\/\]]", output)[1]
    print ("Downloaded " + str(result))
    try:
        os.remove(str(download_dir) + "/" + pfn.split('/')[-1])
    except FileNotFoundError:
        print ("File not found")

    size = convert_size_to_bytes(result)
    duration = time() - ts

    bandwidth = size / duration

    return (size, bandwidth)

def run_cmd(cmd_string):
    proc = subprocess.Popen("/bin/bash -c \"" + cmd_string + "\"", shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    (out, err) = proc.communicate(b'\n')

    return (out.decode("utf-8"), err.decode("utf-8"))


def convert_size_to_bytes(size_str):
    multipliers = {
        'kilobyte':  1024,
        'megabyte':  1024 ** 2,
        'gigabyte':  1024 ** 3,
        'terabyte':  1024 ** 4,
        'petabyte':  1024 ** 5,
        'exabyte':   1024 ** 6,
        'zetabyte':  1024 ** 7,
        'yottabyte': 1024 ** 8,
        'kb': 1024,
        'mb': 1024**2,
        'gb': 1024**3,
        'tb': 1024**4,
        'pb': 1024**5,
        'eb': 1024**6,
        'zb': 1024**7,
        'yb': 1024**8,
    }

    for suffix in multipliers:
        size_str = size_str.lower().strip().strip('s')
        if size_str.lower().endswith(suffix):
            return int(float(size_str[0:-len(suffix)]) * multipliers[suffix])
    else:
        if size_str.endswith('b'):
            size_str = size_str[0:-1]
        elif size_str.endswith('byte'):
            size_str = size_str[0:-4]
    return int(size_str)


suffixes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB']
def humansize(nbytes):
    i = 0
    while nbytes >= 1024 and i < len(suffixes)-1:
        nbytes /= 1024.
        i += 1
    f = ('%.2f' % nbytes).rstrip('0').rstrip('.')
    return '%s %s' % (f, suffixes[i])


def setup_download_dir():
    download_dir = Path('/tmp/filecrawling')
    if not download_dir.exists():
        download_dir.mkdir()

    return download_dir
