# File crawler

## Usage

python3 main.py \<thread-number\> \<file-number\> \[\<path-to-decoded-pfns\>\]

## Usage on the CORI supercomputer

For the CORI supercomputer we have saved the decoded PFNs in the `pfn_decoded.txt` file in the home directory of the `aliprod` user.

For running the test using SLURM, you can run the `fileCrawlerLauncher.sh` batch file using the `sbatch fileCrawlerLauncher.sh` command.
