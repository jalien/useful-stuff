#!/bin/bash
#SBATCH --time=00:30:00
#SBATCH --nodes=10
#SBATCH --partition=debug
#SBATCH --no-requeue
#SBATCH --constraint=haswell
#SBATCH --image=docker:martabertran88/file-crawling:latest
#SBATCH --module=cvmfs

srun -c 62 --ntasks=10 shifter python3 main.py 62 1000
